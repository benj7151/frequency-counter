document.getElementById("countButton").onclick = function() {
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    // This changes all the letter to lower case.
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    // This gets rid of all the characters except letters, spaces, and apostrophes.
    // We will learn more about how to use the replace function in an upcoming lesson.
    let letterCount = [];
    const words = typedText.split(" ");

    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if(letterCount[currentLetter] === undefined) {
            letterCount[currentLetter] = 1;
         } else {
            letterCount[currentLetter]++;
         }

    }
    for (let letter in letterCount) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCount[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
        }
    const word = document.createTextNode('There are ' + (words.length) + ' words');
    document.getElementById("wordsDiv").appendChild(word)   
}
